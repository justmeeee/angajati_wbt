package AngajatiApp.repository;

import AngajatiApp.model.DidacticFunction;
import AngajatiApp.model.Employee;
import AngajatiApp.validator.EmployeeException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class EmployeeMockTest {

    private EmployeeMock empl;

    @Before
    public void setUp() throws Exception {
        empl = new EmployeeMock();
        System.out.println("Before test");
    }

    @After
    public void tearDown() throws Exception {
        empl = null;
        System.out.println("After test");
    }

    @Test
    public void Test01() {
        Employee e1 = null;
        List<Employee> employeeMockList = empl.getEmployeeList();
        empl.modifyEmployeeFunction(e1, DidacticFunction.ASISTENT);
        assertTrue(empl.getEmployeeList().equals(employeeMockList));
    }


    @Test
    public void Test02() {

        List<Employee> employeeMockList = empl.getEmployeeList();

        Employee e1 = new Employee();
        e1.setLastName("Ion");
        e1.setFirstName("Dumitrescu");
        e1.setCnp("1234567890876");
        e1.setFunction(DidacticFunction.ASISTENT);
        e1.setSalary(2500d);
        empl.addEmployee(e1);
        empl.modifyEmployeeFunction(e1, DidacticFunction.ASISTENT);
        assertTrue(empl.getEmployeeList().equals(employeeMockList));

    }


    @Test
    public void Test03() {

        Employee e1 = new Employee();
        e1.setLastName("Ion");
        e1.setFirstName("Dumitrescu");
        e1.setCnp("1234567890876");
        e1.setFunction(DidacticFunction.ASISTENT);
        e1.setSalary(2500d);
        empl.addEmployee(e1);

        List<Employee> employeeMockList = empl.getEmployeeList();
        empl.modifyEmployeeFunction(e1, DidacticFunction.LECTURER);
        assertTrue(e1.getFunction() == DidacticFunction.LECTURER);
    }




}
